MIPS Assembler

Usage via compile:
1) Install rust via https://rustup.rs/
2) Open Shell/cmd in this folder
3) Run "cargo build"
4) Run "cargo run input.txt output.txt

Usage via executable:
assembler (64bit WSL) and assembler.exe (Windows) are provided, run with "input.txt output.txt" parameters.

Format:

- Instructions use ALLCPAPS
- Registers are $0 to $31
- Comma seperators are optional, `MOV $1 $2 $3` is just as valid as `MOV $1, $2, $3`.
- labels are `text:` and can precede an instruction as in `start: ADDI $1 $0 200` or stand alone.
- Comments start with `//`.

Instruction set:

```
J target
BEQ rs, rt, label
BNE rs, rt, label
ADDI rt, rs, immediate
ADDIU rt, rs, immediate
SLTI rt, rs, immediate
SLTIU rt, rs, immediate
ANDI rt, rs, immediate
ORI rt, rs, immediate
XORI rt, rs, immediate
LW rt, base, immediate
SW rt, base, immediate
SLL rd, rt, sa
SRL rd, rt, sa
SRA rd, rt, sa
SYSCALL
ADD rd, rs, rt
ADDU rd, rs, rt
SUB rd, rs, rt
SUBU rd, rs, rt
AND rd, rs, rt
OR rd, rs, rt
XOR rd, rs, rt
NOR rd, rs, rt
SLT rd, rs, rt
SLTU rd, rs, rt
BLTZ rs, label
BGEZ rs, label
BLEZ rs, label
BGTZ rs, label