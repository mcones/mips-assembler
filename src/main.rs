use logos::Logos;
use std::env;
use std::fs;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::process::exit;


const OPCODE_L: i32 = 26;
const RS_L: i32 = 21;
const RT_L: i32 = 16;
const RD_L: i32 = 11;
const SA_L: i32 = 6;



#[derive(Logos, Debug, PartialEq)]
enum Token {
    #[end]
    End,
    #[error]
    Error,
    #[token = ","]
    Comma,
    #[token = ":"]
    Colon,
    #[regex = r#"[A-Za-z_]+"#]
    Text,
    #[regex = "(ADD|ADDU|SUB|SUBU|AND|OR|XOR|NOR|SLT|SLTU|SLL|SRL|SRA|ADDI|ADDIU|ANDI|ORI|XORI|SLTI|SLTIU|BEQ|BNE|BLEZ|BGEZ|BLTZ|BGTZ|LW|SW|J|SYSCALL|NOP)"]
    Instruction,
    //#[regex = r#"\$((zero)|(at)|([gsp]p)|(ra)|([vk][0-1])|(a[0-3])|(s[0-7])|(t[0-9]))"#]
    #[regex = r#"\$[0-9][0-9]?"#]
    Register,
    #[regex = r#"-?[0-9]+"#]
    Offset,

    #[token = "//"]
    Comment
}

#[derive(Debug)]
struct Word {
    operation: String,
    regs: Vec<i32>,
    offset: i16,
    label: String,
}


fn parse(i: usize, line: &str) -> (Option<Word>, Option<String>) {
    let mut label_option : Option<String> = None;
    let mut lexer = Token::lexer(line);
    // Check if label
    if lexer.token == Token::Text {
        let text: &str = lexer.slice();
        lexer.advance();
        if lexer.token == Token::Colon{
            label_option = Some(text.to_string());
            lexer.advance();
        }
    }
    // Check if instruction
    if lexer.token != Token::End && lexer.token != Token::Comment {
        if lexer.token != Token::Instruction {
            panic!("Error: Expected instruction in line {}, got '{}'",i, lexer.slice());
        }
        // Save instruction and advance
        let ins: String = lexer.slice().to_string();
        lexer.advance();
        // Save registers
        let mut regs: Vec<i32> = Vec::new();
        let mut offset: i16 = 0;
        let mut label: String = "".to_string();
        while lexer.token != Token::End && lexer.token != Token::Comment {
            if lexer.token == Token::Register{
                let regnum: i32 = lexer.slice()[1..].parse().unwrap();
                regs.push(regnum);
            }
            if lexer.token == Token::Offset{
                if &lexer.slice()[0..1] == "-" {
                    offset = lexer.slice()[1..].parse().unwrap();
                    offset *= -1;
                } else{
                    offset = lexer.slice().parse().unwrap();
                }
                
            }
            if lexer.token == Token::Text{
                label = lexer.slice().to_string();

            }
            lexer.advance();
            if lexer.token == Token::Comma {lexer.advance();}
        }
        let w = Word {
            operation: ins,
            regs: regs,
            offset: offset,
            label: label,
        };
        return (Some(w), label_option);
    }
   return (None, label_option);
    
}
            
fn assemble(i:usize, w:&Word, l:&HashMap<String, usize>)-> i32{
    match w.operation.as_ref(){
        // Jump
        "J" => ins_jump(&w, &l),
        // Immediate: rs, rt, offset
        "BEQ"| "BNE"| "ADDI"| "ADDIU"| "SLTI"|
        "SLTIU"| "ANDI"| "ORI"| "XORI"| "LW"| "SW" =>  ins_immed(i,&w, &l),
        // INS rd, rt, sa
        "SLL"|"SRL"|"SRA"|"ADD"|"ADDU"|"SUB"|
        "SUBU"|"AND"|"OR"|"XOR"|"NOR"|"SLT"|"SLTU" =>  ins_reg3(&w),
        // Syscall
        "SYSCALL"|"NOP" => ins_sys(&w),
        // RI
        "BLTZ"|"BGEZ"|"BLEZ"|"BGTZ" => ins_zbranch(i, &l, &w),
        &_ => 0

    }

}


fn ins_jump(w: &Word, l:&HashMap<String, usize> ) -> i32{
    // TODO: error checking
    // jump and offset
    let mut ins:i32 = 0;
    ins += 2 << OPCODE_L;
    ins += (translate_label_j(l.get(&w.label)) as u16) as i32;
    ins

    
}
fn ins_immed(i:usize,w: &Word, l:&HashMap<String, usize>) -> i32{
    // TODO: error checking
    // beq/bne + offset or op with immediate
    let mut ins:i32 = 0;
    ins += translate_op_code(&w.operation) << OPCODE_L;
    ins += w.regs[0] << RT_L;
    ins += w.regs[1] << RS_L;
    //println!("{}", detect_branch(i,&l, &w) as i16);
    ins += detect_branch(i,&l, &w);
    ins

}   
fn ins_reg3(w: &Word) -> i32{
    // TODO: error checking
    // fn_op and rd/rt/sa or rd/rs/rt
    let mut ins:i32 = 0;
    ins += w.regs[0] << RD_L;
    ins += detect_shift(&w);
    ins += translate_fn_code(&w.operation);
    ins
}


fn ins_sys(w: &Word) -> i32{
    match w.operation.as_ref(){
        "SYSCALL" => 12,
        &_ => 37
    }
}
fn ins_zbranch(i: usize, l:&HashMap<String, usize>, w: &Word) -> i32{
    // TODO: error checking
    let mut ins:i32 = 0;
    ins += match w.operation.as_ref(){
        "BLEZ" => 6 << OPCODE_L,
        "BGTZ" => 7 << OPCODE_L,
        _ => 1 << OPCODE_L
    };
    ins += detect_ri(&w.operation) << RT_L;
    ins += w.regs[0] << RS_L;
    ins += (translate_label(i, l.get(&w.label)) as u16) as i32;
    ins
}

fn detect_branch(i:usize, l:&HashMap<String, usize>, w: &Word) -> i32{
    match w.operation.as_ref(){
        "BEQ"|"BNE" => (translate_label(i, l.get(&w.label)) as u16) as i32,
        _ => (w.offset as u16) as i32
    }
}

fn detect_ri(w: &String)-> i32{
    match w.as_ref(){
        "BGEZ" => 1,
        _ => 0 
    }
}

fn detect_shift(w: &Word) -> i32{
    match w.operation.as_ref(){
        "SLL"|"SRL"|"SRA" => (w.regs[1] << RT_L) + (w.regs[2] << SA_L),
        _ => (w.regs[1] << RS_L) + (w.regs[2] << RT_L)
    }
}

fn translate_fn_code(w: &String) -> i32{
    match w.as_ref(){
        "SLL" =>	0,
        "SRL" =>	1,
        "SRA" =>	2,
        "SYSCALL" =>	12,
        "ADD" =>	37,
        "ADDU" =>	38,
        "SUB" =>	39,
        "SUBU" =>	32,
        "AND" =>	33,
        "OR" =>	    34,
        "XOR" =>	35,
        "NOR" =>	36,
        "SLT" =>	45,
        "SLTU" =>	46,
        _ => 0
    }
}


fn translate_op_code(w: &String) -> i32{
    match w.as_ref(){
        "BEQ" =>	4,
        "BNE" =>	5,
        "BLEZ" =>	6,
        "BGTZ" =>	7,
        "ADDI" =>	8,
        "ADDIU" =>	9,
        "SLTI" =>	10,
        "SLTIU" =>	11,
        "ANDI" =>	12,
        "ORI" =>	13,
        "XORI" =>	14,
        "LW" => 	35,
        "SW" =>	    43,
        _ => 0
    }
}

fn translate_label(i:usize, s: Option<&usize>) -> i16{
    let i_u: i16 = i as i16;
    match s{
        None => 0,
        Some(x) => ((*x as i16) - i_u - 1)
    }
}

fn translate_label_j(s: Option<&usize>) -> i16{
    match s{
        None => 0,
        Some(x) => (*x as i16)  << 2
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut words: Vec<Word> = Vec::new();
    let mut labels = HashMap::new();
    let mut _j:Option<usize>;

    let (filename, nopflag) = parse_config(&args);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    



    for (i, line) in contents.lines().enumerate(){
        let (word_option, label_option) = parse(i, line);
        match label_option{
            Some(l) => _j = labels.insert(l , words.len() ),
            None => (),
        } 
        match word_option{
            Some(w) => words.push(w),
            None => (),
        }
    }


    if args.len() > 2{
        let output = File::create(&args[2]).unwrap();
        let mut writer = BufWriter::new(&output);
        for (i,w) in words.iter().enumerate(){
            let s:String = format!("instruction_mem.write({}, {:#X})",i*4, assemble(i, w, &labels));
             writeln!(&mut writer,"{}",s);
    
            }

    } else{
        for (i,w) in words.iter().enumerate(){
            //print!("{}: {:?}, ",i,w);
            let s:String = format!("instruction_mem.write({}, {:#X})",i*4, assemble(i, w, &labels));
             println!("{}",s);
    
            }

    }

        

}

fn parse_config(args: &[String]) -> (&str, Option<&String>) {
    if args.len() < 2 {
        println!("Usage: assembler input.txt output.txt");
        exit(0);
        
    }
    let filename = &args[1];
    let nopflag = args.get(3);

    (filename, nopflag)
}